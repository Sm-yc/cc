[TOC]

## 虚拟化技术KVM

#### 一、虚拟化技术概述

![](./assets/1.jpg)



虚拟化[Virtualization]技术最早出现在 20 世纪 60 年代的 IBM ⼤型机系统，在70年代的 System370 系列中逐渐流⾏起来，这些机器通过⼀种叫虚拟机监控器[Virtual Machine Monitor，VMM]的程序物理硬件之上⽣成许多可以运⾏独⽴操作系统软件的虚拟机[Virtual Machine]实例。随着近年多核系统、集群、⽹格甚⾄云计算的⼴泛部署，虚拟化技术在商业应⽤上的优势⽇益体现，不仅降低了 IT 成本，⽽且还增强了系统安全性和可靠性，虚拟化的概念也逐渐深⼊到⼈们⽇常的⼯作与⽣活中。

虚拟化是⼀个⼴义的术语，对于不同的⼈来说可能意味着不同的东⻄，这要取决他们所处的环境。在计算机科学领域中，虚拟化代表着对计算资源的抽象，⽽不仅仅局限于虚拟机的概念。例如对物理内存的抽象，产⽣了虚拟内存技术，使得应⽤程序认为其⾃身拥有连续可⽤的地址空间[Address Space]。⽽实际上，应⽤程序的代码和数据可能是被分隔成多个碎⽚⻚或段），甚⾄被交换到磁盘、闪存等外部存储器上，即使物理内存不⾜，应⽤程序也能顺利执⾏。

#### 二、主流虚拟化方案介绍

##### 1、虚拟化技术主要分类

- 平台虚拟化（Platform Virtualization）

  针对计算机和操作系统的虚拟化。

- 资源虚拟化（Resource Virtualization）

  针对特定的系统资源的虚拟化，⽐如内存、存储、⽹络资源等。

- 应用程序虚拟化（Application Virtualization）

  包括仿真、模拟、解释技术等。

##### 2、平台虚拟化技术分类

我们通常所说的虚拟化主要是指平台虚拟化技术，通过使⽤控制程序（Control Program，也被称为Virtual Machine Monitor 或Hypervisor），隐藏特定计算平台的实际物理特性，为⽤户提供抽象的、统⼀的、模拟的计算环境（称为虚拟机）。虚拟机中运⾏的操作系统被称为客户机操作系统（GuestOS），运⾏虚拟机监控器的操作系统被称为主机操作系统（Host OS），当然某些虚拟机监控器可以脱离操作系统直接运⾏在硬件之上（如 VMWARE 的 ESX 产品）。运⾏虚拟机的真实系统我们称之为主机系统。



**操作系统级虚拟化（Operating System Level Virtualization）**



>　　在传统操作系统中，所有⽤户的进程本质上是在同⼀个操作系统的实例中运⾏，因此内核或应⽤程
>
>序的缺陷可能影响到其它进程。操作系统级虚拟化是⼀种在服务器操作系统中使⽤的轻量级的虚拟化技
>
>术，内核通过创建多个虚拟的操作系统实例（内核和库）来隔离不同的进程，不同实例中的进程完全不
>
>了解对⽅的存在。 
>
>　　⽐较著名的有 Solaris Container，FreeBSD Jail 和 OpenVZ 等。 
>
>　　⽐如OPENVZ：这个平台是最便宜的VPS平台，在各个vps商⾥都是价格最低的。 
>
>OPENVZ本身运⾏在linux之上，它通过⾃⼰的虚拟化技术把⼀个服务器虚拟化成多个可以分别安装操作
>
>系统的实例，这样的每⼀个实体就是⼀个VPS，从客户的⻆度来看这就是⼀个虚拟的服务器，可以等同
>
>看做⼀台独⽴的服务器。 
>
>　　OPENVZ虚拟化出来的VPS只能安装linux操作系统，不能安装windows系统，⽐如 Centos、
>
>Fedora、Gentoo、Debian等。不能安装windows操作系统是openvz的第⼀个缺点、需要使⽤windows
>
>平台的⽤户不能使⽤OPENVZVPS。 
>
>　　OPENVZ的第⼆个缺点是OPENVZ不是完全的虚拟化，每个VPS账户共⽤⺟机内核，不能单 独修改内
>
>核。好在绝⼤多数⽤户根本不需要修改内核，所以这个缺点对多数⼈可以忽略不计。 ⽽这⼀点也正是
>
>openvz的优点，这⼀共⽤内核特性使得openvz的效率最⾼，超过KVM、 Xen、VMware等平台。在不超
>
>售的情况下，openvz是最快速效率最⾼的VPS平台。



**部分虚拟化（Partial Virtualization）**



>　　VMM 只模拟部分底层硬件，因此客户机操作系统不做修改是⽆法在虚拟机中运⾏的，其它程序可能
>
>也需要进⾏修改。在历史上，部分虚拟化是通往全虚拟化道路上的重要⾥程碑,最早出现在第⼀代的分时
>
>系统 CTSS 和 IBM M44/44X 实验性的分⻚系统中。



**全虚拟化（Full Virtualization）**



>　　全虚拟化是指虚拟机模拟了完整的底层硬件，包括处理器、物理内存、时钟、外设等，使得为原始
>
>硬件设计的操作系统或其它系统软件完全不做任何修改就可以在虚拟机中运⾏。
>
>　　操作系统与真实硬件之间的交互可以看成是通过⼀个预先规定的硬件接⼝进⾏的。全虚拟化 VMM 
>
>以完整模拟硬件的⽅式提供全部接⼝（同时还必须模拟特权指令的执⾏过程）。举例⽽⾔，x86 体系结
>
>构中，对于操作系统切换进程⻚表的操作，真实硬件通过提供⼀个特权CR3 寄存器来实现该接⼝，操作
>
>系统只需执⾏ "mov pgtable,%%cr3"汇编指令即可。
>
>　　全虚拟化 VMM 必须完整地模拟该接⼝执⾏的全过程。如果硬件不提供虚拟化的特殊⽀持，那么这
>
>个模拟过程将会⼗分复杂：⼀般⽽⾔，VMM 必须运⾏在最⾼优先级来完全控制主机系统，⽽ Guest OS 
>
>需要降级运⾏，从⽽不能执⾏特权操作。当 Guest OS 执⾏前⾯的特权汇编指令时，主机系统产⽣异常
>
>（General Protection Exception），执⾏控制权重新从 Guest OS转到 VMM ⼿中。VMM 事先分配⼀个
>
>变量作为影⼦ CR3 寄存器给 Guest OS，将 pgtable 代表的客户机物理地址（Guest Physical Address）
>
>填⼊影⼦ CR3 寄存器，然后 VMM 还需要pgtable 翻译成主机物理地址（Host Physical Address）并填
>
>⼊物理 CR3 寄存器，最后返回到 Guest OS中。随后 VMM 还将处理复杂的 Guest OS 缺⻚异常（Page 
>
>Fault）。
>
>　　⽐较著名的全虚拟化 VMM 有 Microsoft Virtual PC、VMware Workstation、Sun Virtual、Box、
>
>Parallels Desktop for Mac 和 QEMU。



*硬件辅助虚拟化（Hardware-Assisted Virtualization）*



>　　硬件辅助虚拟化是指借助硬件（主要是主机处理器）的⽀持来实现⾼效的全虚拟化。例如有了Intel-
>
>VT 技术的⽀持，Guest OS 和 VMM 的执⾏环境⾃动地完全隔离开来，Guest OS 有⾃⼰的""套寄存器"，
>
>可以直接运⾏在最⾼级别。因此在上⾯的例⼦中，Guest OS 能够执⾏修改⻚表的汇编指令。Intel-VT 和 
>
>AMD-V 是⽬前 x86 体系结构上可⽤的两种硬件辅助虚拟化技术。



这种分类并不是绝对的，⼀个优秀的虚拟化软件往往融合了多项技术。例如 VMware Workstation是⼀个著名的全虚拟化的 VMM，但是它使⽤了⼀种被称为动态⼆进制翻译的技术把对特权状态的访问转换成对影⼦状态的操作，从⽽避免了低效的 Trap-And-Emulate 的处理⽅式，这与超虚拟化相似，只不过超虚拟化是静态地修改程序代码。对于超虚拟化⽽⾔，如果能利⽤硬件特性，那么虚拟机的管理将会⼤⼤简化，同时还能保持较⾼的性能。

#### 三、KVM虚拟化技术简介

CSDN[虚拟化技术的实现](https://blog.csdn.net/gui951753/article/details/81507924)

##### 1.KVM架构

![](./assets/2.jpeg)

##### 2.KVM架构解析

从rhel6开始使⽤，红帽公司直接把KVM的模块做成了内核的⼀部分。

xen⽤在rhel6之前的企业版中默认内核不⽀持，需要重新安装带xen功能的内核KVM 针对运⾏在 x86 硬件上的、驻留在内核中的虚拟化基础结构。KVM 是第⼀个成为原⽣ Linux 内核（2.6.20）的⼀部分的 hypervisor，它是由 Avi Kivity 开发和维护的，现在归 Red Hat 所有。

这个 hypervisor 提供 x86 虚拟化，同时拥有到 PowerPC® 和 IA64 的通道。另外，KVM 最近还添加了对对称多处理（SMP）主机（和来宾）的⽀持，并且⽀持企业级特性，⽐如活动迁移（允许来宾操作系统在物理服务器之间迁移）。

KVM 是作为内核模块实现的，因此 Linux 只要加载该模块就会成为⼀个hypervisor。KVM 为⽀持hypervisor 指令的硬件平台提供完整的虚拟化（⽐如 Intel® Virtualization Technology [Intel VT] 或AMD Virtualization [AMD-V] 产品）。KVM 还⽀持准虚拟化来宾操作系统，包括 Linux 和 Windows®。

这种技术由两个组件实现。第⼀个是可加载的 KVM 模块，当在 Linux 内核安装该模块之后，它就可以管理虚拟化硬件，并通过 /proc ⽂件系统公开其功能。第⼆个组件⽤于 PC 平台模拟，它是由修改版QEMU 提供的。QEMU 作为⽤户空间进程执⾏，并且在来宾操作系统请求⽅⾯与内核协调。

当新的操作系统在 KVM 上启动时（通过⼀个称为 KVM 的实⽤程序），它就成为宿主操作系统的⼀个进程，因此就可以像其他进程⼀样调度它。但与传统的 Linux 进程不⼀样，来宾操作系统被 hypervisor标识为处于 "来宾" 模式（独⽴于内核和⽤户模式）。

每个来宾操作系统都是通过 /dev/KVM 设备映射的，它们拥有⾃⼰的虚拟地址空间，该空间映射到主机内核的物理地址空间。如前所述，KVM 使⽤底层硬件的虚拟化⽀持来提供完整的（原⽣）虚拟化。I/O请求通过主机内核映射到在主机上（hypervisor）执⾏的 QEMU 进程。

KVM 在 Linux 环境中以主机的⽅式运⾏，不过只要底层硬件虚拟化⽀持，它就能够⽀持⼤量的来宾操作系统。

|                  | KVM                            |
| ---------------- | ------------------------------ |
| 问世时间         | 2007年                         |
| 支持企业         | RedHat、Ubuntu等               |
| 支持的虚拟化技术 | 全虚拟化                       |
| 支持架构         | 支持虚拟化的CPU                |
| 支持操作系统     | UNIX、Linux和Microsoft Windows |
| 动态迁移         | 支持（以前不支持）             |
| 内核支持         | 内置在内核中                   |

#### 四、KVM软件安装

##### 1、环境准备

##### 2、查看CPU是否支持VT技术

```
[root@localhost ~]# cat /proc/cpuinfo | grep -E 'vmx|svm'
```

##### 3、清理环境：卸载KVM

```
[root@localhost ~]# yum remove `rpm -qa | egrep 'qemu|virt|KVM'` -y
[root@localhost ~]# rm -rf /var/lib/libvirt /etc/libvirt/
```

##### 4、安装软件

##### Centos6

```
[root@localhost ~]# yum groupinstall "Virtualization" "Virtualization
Client" "Virtualization Platform" "Virtualization Tools" -y
```

##### Centos7

```
[root@localhost ~]# yum install *qemu* *virt* librbd1-devel -y
```

>qemu-KVM： 主包
>　　libvirt：API接⼝
>　　virt-manager：图形管理程序

##### Centos8

>yum 组安装已经可以使⽤了
>       问题：在进⾏组安装的时候会出现关于rpm版本的错误问题
>       解决：
>
>```
>#yum upgrade rpm -y
>```

组安装成功之后还是要单独安装virt-manager⼯具

##### 5、启动服务

```
[root@localhost ~]# systemctl start libvirtd
```

##### 6、查看KVM模块加载

```
[root@localhost ~]# lsmod | grep KVM
KVM_intel 53484 3
KVM 316506 1 KVM_intel
```











